package com.korba.surfline.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.korba.surfline.db.TransactionLogs;

@Repository
public interface TransactionLogsRepository extends CrudRepository<TransactionLogs, Long>{
	TransactionLogs findByKorbaTransId(String korbaTransId);
}
