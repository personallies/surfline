package com.korba.surfline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SurflineIntegrationV1Application {

	public static void main(String[] args) {
		SpringApplication.run(SurflineIntegrationV1Application.class, args);
	}
}
