package com.korba.surfline.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.korba.surfline.db.TransactionLogs;
import com.korba.surfline.model.BalanceQueryResponse;
import com.korba.surfline.model.BundlePurchaseRequest;
import com.korba.surfline.model.BundlePurchaseResponse;
import com.korba.surfline.model.GetTransaction;
import com.korba.surfline.model.QueryPackageRequest;
import com.korba.surfline.model.QueryPackageResponse;
import com.korba.surfline.model.QueryTransHistRequest;
import com.korba.surfline.model.ResponseCode;
import com.korba.surfline.model.ResponseMessage;
import com.korba.surfline.model.TransactionHistoryResponse;
import com.korba.surfline.properties.ApplicationProperties;
import com.korba.surfline.repository.TransactionLogsRepository;
import com.korba.surfline.service.BusinessLogic;
import com.korba.surfline.utils.JsonUtility;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController 
public class Controller {
	@Autowired
	BusinessLogic businessLogic;
	@Autowired
	TransactionLogsRepository transactionLogsRepository;
	@Autowired
	ApplicationProperties applicationProperties;
	
	@GetMapping("/ping")
	public String ping() {
		return "Welcome! You can now buy your new surfline bundles on Korba";
	}
	
	@PostMapping("/query/package")
	public QueryPackageResponse queryPackage(@Valid @RequestBody QueryPackageRequest request, Errors errors){
		QueryPackageResponse response = new QueryPackageResponse();
		log.info("the json request : "+ JsonUtility.toJson(request));
		if(!errors.hasErrors()) {
			try {
				response = businessLogic.queryPackage(request);
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				response.setResponseCode(ResponseCode.EXCEPTION);
				response.setResponseMessage(ResponseMessage.EXCEPTION +" : "+e.getMessage());
			}
		}else {
			response.setResponseCode(ResponseCode.BAD_REQUEST);
			response.setResponseMessage(ResponseMessage.BAD_REQUEST);
		}
		log.info("the json response : "+ JsonUtility.toJson(response));
		return response;
	}
	
	@PostMapping("/purchase/bundle")
	public BundlePurchaseResponse purchaseBundle(@Valid @RequestBody BundlePurchaseRequest request, Errors errors){
		BundlePurchaseResponse response = new BundlePurchaseResponse();
		log.info("the json request : "+JsonUtility.toJson(request));
		if(!errors.hasErrors()) {
			TransactionLogs duplicateTransaction = transactionLogsRepository.findByKorbaTransId(request.getRefNo());

			if(duplicateTransaction == null) {
				try {
					response = businessLogic.purchaseBundle(request);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					response.setResponseCode(ResponseCode.EXCEPTION);
					response.setResponseMessage(ResponseMessage.EXCEPTION +" : "+e.getMessage());
				}
			}else {
				response.setResponseCode(ResponseCode.DUPLICATE_TRANS);
				response.setResponseMessage(ResponseMessage.DUPLICATE_TRANS);
			}
		}else {
			response.setResponseCode(ResponseCode.BAD_REQUEST);
			response.setResponseMessage(ResponseMessage.BAD_REQUEST);
		}
		log.info("the json response : "+JsonUtility.toJson(response));
		return response;
	}
	
	@PostMapping("/get/transaction")
	public TransactionLogs getTransaction(@Valid @RequestBody GetTransaction getTransaction,Errors errors) {
		TransactionLogs transactionLogs = new TransactionLogs();
		log.info("the json request: "+JsonUtility.toJson(getTransaction));
		if(!errors.hasErrors()) {
			try {
				transactionLogs = transactionLogsRepository.findByKorbaTransId(getTransaction.getRefNo());
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				transactionLogs.setResponseCode(ResponseCode.EXCEPTION);
				transactionLogs.setResponseMessage(ResponseMessage.EXCEPTION +" : "+e.getMessage());
			}
		}else {
			transactionLogs.setResponseCode(ResponseCode.BAD_REQUEST);
			transactionLogs.setResponseMessage(ResponseMessage.BAD_REQUEST);
		}
		log.info("the json response: "+JsonUtility.toJson(transactionLogs));
		return transactionLogs;
	}
	
	@GetMapping("/query/balance")
	public BalanceQueryResponse queryBalance() {
		BalanceQueryResponse balanceQueryResponse = businessLogic.queryBalance();
		log.info("the json response: "+JsonUtility.toJson(balanceQueryResponse));
		return balanceQueryResponse;
	}
	
	@PostMapping("/query/transaction/history")
	public TransactionHistoryResponse queryTransHist(@RequestBody QueryTransHistRequest request) {
		TransactionHistoryResponse transactionHistoryResponse = businessLogic.queryTransactionHistory(request);
		log.info("the json response: "+JsonUtility.toJson(transactionHistoryResponse));
		return transactionHistoryResponse;
	}
}
