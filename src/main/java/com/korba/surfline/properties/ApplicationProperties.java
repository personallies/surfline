package com.korba.surfline.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component 
@ConfigurationProperties
@PropertySource("classpath:application-surfline-production.properties")
public class ApplicationProperties {
	private String idPrefix;
	private String surflineUsername;
	private String surflinePassword;
	private String namespace;
	private String packageQueryUrl;
	private String bundlePurchaseUrl;
	private String ccCallingPartyId;
	private String channel;
	private String recipientWalletType;
	private String requestType;
	private String xmlnsEpar;
	private String xmlnsPac;
	private String tagPrefix;
	private int bundleListStartIndex;
	private String xmlnsPi;
	private String msisdn;
	private String listType;
	private String walletType;
	private String balanceType;
	private String balanceQueryUrl;
	private String transactionHistoryUrl;
	private String piPrefix;
	private String edrType;
	private String maxRecords;
	private String startDate;
}
