package com.korba.surfline.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import lombok.Data;

@Data
@Configuration
@PropertySource("classpath:message.properties")
@ConfigurationProperties
public class MessageProperties {
	private String surfplusMessage;
	private String fupDataMessage;
}
