package com.korba.surfline.model;

public enum ServiceEnum {
	INDIVIDUAL,
	NewPrepaidOffer,
	SurfStaff,
	BusinessIndividual,
	BusinessCorporate

}
