package com.korba.surfline.model;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class QueryPackageRequest {
	@NotNull
	private String msisdn;
}
