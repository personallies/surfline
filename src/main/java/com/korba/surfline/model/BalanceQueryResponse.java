package com.korba.surfline.model;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@Component
public class BalanceQueryResponse extends GeneralResponse{
	@JsonInclude(Include.NON_EMPTY)
	private String auth;
	@JsonInclude(Include.NON_EMPTY)
	private String msisdn;
	@JsonInclude(Include.NON_EMPTY)
	private String accountNumber;
	@JsonInclude(Include.NON_EMPTY)
	private String balance;
	@JsonInclude(Include.NON_EMPTY)
	private String error;
	@JsonInclude(Include.NON_EMPTY)
	private String faultCode;
	@JsonInclude(Include.NON_EMPTY)
	private String faultString;
}
