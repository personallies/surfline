package com.korba.surfline.model;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class TransactionHistoryRequest {
	private String maxRecords;
	private String startDate;
	private String endDate;
}
