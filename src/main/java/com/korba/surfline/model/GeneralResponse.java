package com.korba.surfline.model;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@Component
public class GeneralResponse {
	@JsonInclude(Include.NON_NULL)
	private String responseCode;
	@JsonInclude(Include.NON_NULL)
	private String responseMessage;
}
