package com.korba.surfline.model;

import java.util.List;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@Component
public class QueryPackageResponse extends GeneralResponse{
	@JsonInclude(Include.NON_EMPTY)
	private List<String> bundle;
	@JsonInclude(Include.NON_EMPTY)
	private String accountType;
	@JsonInclude(Include.NON_EMPTY)
	private String result;
	@JsonInclude(Include.NON_EMPTY)
	private String error;
}
