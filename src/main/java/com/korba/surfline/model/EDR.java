package com.korba.surfline.model;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class EDR {
	private String recordDate;
	private String edrType;
	private String walletType;
	private String chargingDomainId;
	private String callId;
	private String scpId;
	private String sequenceNumber;
	private String extraInformation;
}
