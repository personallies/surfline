package com.korba.surfline.model;

public class ResponseMessage {
	public static final String QUERY_SUCCESS = "Package query successful";
	public static final String QUERY_FAIL = "Package query failed";
	public static final String BAL_QUERY_SUCCESS = "Balance query successful";
	public static final String BAL_QUERY_FAIL = "Balance query failed";
	public static final String TRANS_HIST_QUERY_SUCCESS = "Transaction History query successful";
	public static final String TRANS_HIST_QUERY_FAIL = "Transaction History query failed";
	public static final String PURCHASE_SUCCESS = "Bundle purchase successful";
	public static final String PURCHASE_FAIL = "Bundle purchase failed";
	public static final String NUMBER_INVALID = "Number does not exist";
	public static final String BUNDLE_INVALID = "Bundle package does not exist";
	public static final String EXCEPTION = "An exception occured";
	public static final String BAD_REQUEST = "Bad Request. Missing/Invalid Parameter";
	public static final String DUPLICATE_TRANS = "Duplicate Transaction.";
}
