package com.korba.surfline.model;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class InactiveNoResp {
	private String acccountStatus;
}
