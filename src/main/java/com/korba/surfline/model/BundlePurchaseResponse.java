package com.korba.surfline.model;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@Component
public class BundlePurchaseResponse extends GeneralResponse{
	private String korbaTransId;
	@JsonInclude(Include.NON_EMPTY)
	private String surflineTransId;
	@JsonInclude(Include.NON_EMPTY)
	private String error;
}
