package com.korba.surfline.model;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class ActiveUnlimBunResp {
	private String fupNotification;
}
