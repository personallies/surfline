package com.korba.surfline.model;

public class ResponseCode {
	public static final String SUCCESS = "01";
	public static final String FAIL = "100";
	public static final String NUMBER_INVALID = "101";
	public static final String BUNDLE_INVALID = "102";
	public static final String EXCEPTION = "103";
	public static final String BAD_REQUEST = "104";
	public static final String DUPLICATE_TRANS = "105";

}
