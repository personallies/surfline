package com.korba.surfline.model;

public class AppConstants {
	public static final String SURF = "Surf";
	public static final String SURFPLUS = "SurfPlus";
	public static final String BUSINESS_AFFINITY = "Business Affinity";
	public static final String BUSINESS_CORPORATE = "Business Corporate";
	public static final String TV = "TV";
}
