package com.korba.surfline.model;

public class BundleId {
	public static String ONEGBSURF = "1gbolddata";
	public static String FIVEGBSURF = "5gbolddata";
	public static String TENGBSURF = "10gbolddata";
	public static String TWENTYGBSURF = "20gbolddata";
	public static String FIFTYGBSURF = "50gbolddata";
	public static String PTFIVEGBSURFPLUS = "0.5gbnewdata";
	public static String ONEPTFIVEGBSURFPLUS = "1.5gbnewdata";
	public static String FIVEGBSURFPLUS = "5gbnewdata";
	public static String TWELVEGBSURFPLUS = "12gbnewdata";
	public static String TWENTYFIVEGBSURFPLUS = "25gbnewdata";
	public static String FIFTYGBSURFPLUS = "50gbnewdata";
	public static String HUNDREDGBSURFPLUS = "100gbnewdata";
	public static String UNLTDSURFPLUS = "uldaynitedata";
	public static String UNLTDNYTSURFPLUS = "ulnitedata";
	public static String VIDSURFPLUS = "videodata";
	public static String FIVEGBAFFINITY = "5gbbus1";
	public static String TWELVEGBAFFINITY = "12gbbus1";
	public static String TWENTYFIVEGBAFFINITY = "25gbbus1";
	public static String FIFTYGBAFFINITY = "50gbbus1";
	public static String FIFTYGBCORPORATE = "50gbbus2";
	public static String HUNDREDGBCORPORATE = "100gbbus2";
	public static String UNLTDCORPORATE = "ulbus2";
}
