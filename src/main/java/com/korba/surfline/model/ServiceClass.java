package com.korba.surfline.model;

public class ServiceClass {
	public static final String INDIVIDUAL = "INDIVIDUAL";
	public static final String NEWPREPAID = "NewPrepaidOffer";
	public static final String SURFSTAFF = "SurfStaff";
	public static final String BNESSINDIVIDUAL = "BusinessIndividual";
	public static final String BNESSCORPORATE = "BusinessCorporate";
	
}
