package com.korba.surfline.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@Table
@Entity
@Component
public class TransactionLogs {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonInclude(Include.NON_EMPTY)
	private Long id;
	// START TRANSACTIONAL DATA
	@Column
//	@Temporal(TemporalType.TIMESTAMP)
	@JsonInclude(Include.NON_EMPTY)
	private String created;
	@Column
//	@Temporal(TemporalType.TIMESTAMP)
	@JsonInclude(Include.NON_EMPTY)
	private String updated;
	@Column(length = 50)
	@JsonInclude(Include.NON_EMPTY)
	private String msisdn;
	@Column(length = 50)
	@JsonInclude(Include.NON_EMPTY)
	private String amount;
	@Column(length = 50,unique=true)
	@JsonInclude(Include.NON_EMPTY)
	private String korbaTransId;
	@Column(length = 50)
	@JsonInclude(Include.NON_EMPTY)
	private String bundleId;
	@Column(length = 50)
	@JsonInclude(Include.NON_EMPTY)
	private String surflineTransId;
	@Column(length = 50)
	private String responseCode;
	@Column
	private String responseMessage;
	@Column(length = 50)
	@JsonInclude(Include.NON_EMPTY)
	private String accountType;
	@Column(length = 20)
	@JsonInclude(Include.NON_EMPTY)
	private String status;
	@Column(length = 20)
	@JsonInclude(Include.NON_EMPTY)
	private String error;
}
