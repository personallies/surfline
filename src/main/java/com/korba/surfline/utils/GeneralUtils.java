package com.korba.surfline.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.korba.surfline.model.AppConstants;
import com.korba.surfline.model.BundleId;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class GeneralUtils {
	
	public Map splitQueryPackageResponse(List<String> bundleList){
		Map<String, String> bundleMap = new HashMap<String, String>();
		log.info("the bundleList : "+bundleList);
		
		for (String bundle : bundleList){
			log.info("the bundle now"+bundle);
			String[] bundleArr = bundle.split("\\|");
			String acctType = bundleArr[0];
			log.info("the acctType"+acctType);
			String amt = bundleArr[1];
			String period = bundleArr[2];
			String bundleId = bundleArr[3];
			log.info("the amt" +amt);
			log.info("the bundle array"+bundleArr);
			bundleMap.put(amt, bundleId);
		}
		log.info("the map : "+bundleMap);
		return bundleMap;
	}
	
	public String getBundleId(String accountType, String amount) {
		String bundleId = "";

		switch (accountType) {
		case AppConstants.SURF: {
			switch (amount) {
			case "25": {
				bundleId = BundleId.ONEGBSURF;
				break;
			}
			case "75": {
				bundleId = BundleId.FIVEGBSURF;
				break;
			}
			case "125": {
				bundleId = BundleId.TENGBSURF;
				break;
			}
			case "205": {
				bundleId = BundleId.TWENTYGBSURF;
				break;
			}
			case "445": {
				bundleId = BundleId.FIFTYGBSURF;
				break;
			}
			}
		}
		case AppConstants.SURFPLUS: {
			switch (amount) {
			case "9.9": {
				bundleId = BundleId.PTFIVEGBSURFPLUS;
				break;
			}
			case "19": {
				bundleId = BundleId.ONEPTFIVEGBSURFPLUS;
				break;
			}
			case "29": {
				bundleId = BundleId.FIVEGBSURFPLUS;
				break;
			}
			case "69": {
				bundleId = BundleId.TWELVEGBSURFPLUS;
				break;
			}
			case "129": {
				bundleId = BundleId.TWENTYFIVEGBSURFPLUS;
				break;
			}
			case "239": {
				bundleId = BundleId.FIFTYGBSURFPLUS;
				break;
			}
			case "399": {
				bundleId = BundleId.HUNDREDGBSURFPLUS;
				break;
			}
			case "299": {
				bundleId = BundleId.UNLTDSURFPLUS;
				break;
			}
			case "99": {
				bundleId = BundleId.UNLTDNYTSURFPLUS;
			}
			}
		}
		case AppConstants.BUSINESS_AFFINITY:{
			switch(amount){
			case "25":{
				bundleId = BundleId.FIVEGBAFFINITY;
				break;
			}
			case "69":{
				bundleId = BundleId.TWELVEGBAFFINITY;
				break;
			}
			case "129":{
				bundleId = BundleId.TWENTYFIVEGBAFFINITY;
				break;
			}
			case "239":{
				bundleId = BundleId.FIFTYGBAFFINITY;
				break;
			}
			}
		}
		case AppConstants.BUSINESS_CORPORATE:{
			switch(amount){
			case "239":{
				bundleId = BundleId.FIFTYGBCORPORATE;
				break;
			}
			case "399":{
				bundleId = BundleId.HUNDREDGBCORPORATE;
				break;
			}
			}
		}
		}
		
		return bundleId;
	}

}
