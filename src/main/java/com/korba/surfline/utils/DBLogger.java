package com.korba.surfline.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.korba.surfline.db.TransactionLogs;
import com.korba.surfline.model.BundlePurchaseRequest;
import com.korba.surfline.model.BundlePurchaseResponse;
import com.korba.surfline.model.Status;
import com.korba.surfline.repository.TransactionLogsRepository;

@Service
public class DBLogger {
	@Autowired
	TransactionLogsRepository transactionLogsRepository;
	
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	public TransactionLogs logTransaction(String korbaTransId, BundlePurchaseRequest bundlePurchaseRequest) {
		TransactionLogs newTransaction = new TransactionLogs();
		TransactionLogs savedTransaction = new TransactionLogs();
		
		newTransaction.setCreated(dateFormat.format(new Date()));
		newTransaction.setKorbaTransId(korbaTransId);
		newTransaction.setMsisdn(bundlePurchaseRequest.getMsisdn());
		newTransaction.setAmount(bundlePurchaseRequest.getAmount());
		newTransaction.setStatus(Status.CREATED);
		
		savedTransaction = transactionLogsRepository.save(newTransaction);
		return savedTransaction;
	}
	
	public void updateTransaction(TransactionLogs foundTransaction, BundlePurchaseResponse bundlePurchaseResponse) {
		foundTransaction.setUpdated(dateFormat.format(new Date()));
		foundTransaction.setSurflineTransId(bundlePurchaseResponse.getSurflineTransId());
		foundTransaction.setError(bundlePurchaseResponse.getError());
		foundTransaction.setResponseCode(bundlePurchaseResponse.getResponseCode());
		foundTransaction.setResponseMessage(bundlePurchaseResponse.getResponseMessage());
		foundTransaction.setStatus(Status.UPDATED);
		
		transactionLogsRepository.save(foundTransaction);
	}
}
