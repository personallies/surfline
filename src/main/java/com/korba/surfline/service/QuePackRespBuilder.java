package com.korba.surfline.service;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.korba.surfline.model.QueryPackageResponse;
import com.korba.surfline.properties.ApplicationProperties;
import com.korba.surfline.utils.SOAPMessageReader;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class QuePackRespBuilder {
	@Autowired
	ApplicationProperties applicationProperties;
	@Autowired
	SOAPMessageReader reader;

	public int count(final String str, final String subStr) {
		int count = 0;
		int idx = 0;

		while ((idx = str.indexOf(subStr, idx)) != -1) {
			idx++;
			count++;
		}

		return count;
	}

	public QueryPackageResponse producePacQueryResponse(String responseXml, String namespace) {
		QueryPackageResponse response = new QueryPackageResponse();
		List<String> tmpBundleList = new ArrayList<>();
		String bundle = "";
		String accountType = "";
		String result = "";
		String error = "";

		try {
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
			Document document = documentBuilder.parse(new InputSource(new StringReader(responseXml)));
			document.getDocumentElement().normalize();

			NodeList errorList = document.getElementsByTagName("erorr");
			if(errorList.item(0) != null) {
				if (errorList.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element errorElement = (Element) errorList.item(0);
					error = errorElement.getTextContent();
					log.info(errorElement.getNodeName() + " : " + errorElement.getTextContent());
				}
			}

			NodeList resultList = document.getElementsByTagName("Result");
			if(resultList.item(0) != null) {
				if (resultList.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element resultElement = (Element) resultList.item(0);
					result = resultElement.getTextContent();
					log.info(resultElement.getNodeName() + " : " + resultElement.getTextContent());
				}
			}

			int length = count(responseXml, namespace);
			log.info("this the length of the bundle list: "+String.valueOf(length));
			if (length > 0) {
				NodeList accountTypeList = document.getElementsByTagName("AccountType");
				if(accountTypeList.item(0) != null) {
					if (accountTypeList.item(0).getNodeType() == Node.ELEMENT_NODE) {
						Element accountTypeElement = (Element) accountTypeList.item(0);
						accountType = accountTypeElement.getTextContent();
						log.info(accountTypeElement.getNodeName() + " : " + accountTypeElement.getTextContent());
					}
				}

				for (int i = applicationProperties.getBundleListStartIndex(); i < length+applicationProperties.getBundleListStartIndex()+1; i++) {
					NodeList bundleList = document.getElementsByTagName("bundle" + (i));

					if (bundleList.item(0) != null) {
						if (bundleList.item(0).getNodeType() == Node.ELEMENT_NODE) {
							Element bundleElement = (Element) bundleList.item(0);
							bundle = bundleElement.getTextContent();
							tmpBundleList.add(bundle);
							log.info(bundleElement.getNodeName() + " : " + bundleElement.getTextContent());
						}
					}
				}
			}

			response.setBundle(tmpBundleList);
			response.setAccountType(accountType);
			response.setResult(result);
			response.setError(error);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return response;

	}
}
