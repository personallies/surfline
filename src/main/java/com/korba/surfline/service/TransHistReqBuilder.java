package com.korba.surfline.service;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.korba.surfline.properties.ApplicationProperties;

@Service
public class TransHistReqBuilder {
	@Autowired
	ApplicationProperties applicationProperties;
	
//	public static void main(String[] args) {
//		TransHistReqBuilder requestBuilder = new TransHistReqBuilder();
//		requestBuilder.getXmlRequest("Kel","Kel","Kel","Kel","Kel","Kel","Kel","Kel");
//	}

	public String getXmlRequest(String vUsername,String vPassword,String vMsisdn,String vWalletType,String vEdrType,String vMaxRecords,String vStartDate,String vEndDate){
		String requestXml="";
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			DOMImplementation impl = builder.getDOMImplementation();

			Document message = impl.createDocument(null, null, null);
			
			Element envelope = message.createElement("soapenv:Envelope");
			envelope.setAttribute("xmlns:soapenv","http://schemas.xmlsoap.org/soap/envelope/");
			envelope.setAttribute("xmlns:pi", "http://xmlns.oracle.com/communications/ncc/2009/05/15/pi");
//			envelope.setAttribute("xmlns:pi", applicationProperties.getXmlnsPi());
			message.appendChild(envelope);
			
			Element header = message.createElement("soapenv:Header");
			envelope.appendChild(header);
			
			Element body = message.createElement("soapenv:Body");
			envelope.appendChild(body);
			
			Element transHistRequest = message.createElement("pi:CCSCD7_QRY");
			body.appendChild(transHistRequest);
			
			Element username = message.createElement("pi:username");
			username.setTextContent(vUsername);
			transHistRequest.appendChild(username);
			
			Element password = message.createElement("pi:password");
			password.setTextContent(vPassword);
			transHistRequest.appendChild(password);
			
			Element msisdn = message.createElement("pi:MSISDN");
			msisdn.setTextContent(vMsisdn);
			transHistRequest.appendChild(msisdn);
			
			Element walletType = message.createElement("pi:WALLET_TYPE");
			walletType.setTextContent(vWalletType);
			transHistRequest.appendChild(walletType);
			
			Element edrType = message.createElement("pi:EDR_TYPE");
			edrType.setTextContent(vEdrType);
			transHistRequest.appendChild(edrType);
			
			Element maxRecords = message.createElement("pi:MAX_RECORDS");
			maxRecords.setTextContent(vMaxRecords);
			transHistRequest.appendChild(maxRecords);
			
			Element startDate = message.createElement("pi:START_DATE");
			startDate.setTextContent(vStartDate);
			transHistRequest.appendChild(startDate);
			
			Element endDate = message.createElement("pi:END_DATE");
			endDate.setTextContent(vEndDate);
			transHistRequest.appendChild(endDate);
			
			DOMSource domSource = new DOMSource(message);
			TransformerFactory tf = TransformerFactory.newInstance();
			
			Transformer transformer = tf.newTransformer();
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

			java.io.StringWriter sw = new java.io.StringWriter();
			StreamResult sr = new StreamResult(sw);
			transformer.transform(domSource, sr);
			requestXml = sw.toString();

			System.out.println(requestXml);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return requestXml;
	}
	

}
