package com.korba.surfline.service;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.korba.surfline.properties.ApplicationProperties;

@Service
public class BundPurcReqBuilder {
	@Autowired
	ApplicationProperties applicationProperties;
	
//	public static void main(String[] args) {
//		 BundPurcReqBuilder bundPurcReqBuilder = new BundPurcReqBuilder();
//		 bundPurcReqBuilder.getXmlRequest("kel", "kel", "kel", "kel", "kel", "kel", "kel");
//	}
	
	public String getXmlRequest(String vCCCallingPartyId, String vChannel, String vTransactionId, String vReceipientNumber, String vReceipientWalletType, String vAmount, String vRequestType){
		String responseXml="";
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			DOMImplementation impl = builder.getDOMImplementation();

			Document message = impl.createDocument(null, null, null);
			
			Element envelope = message.createElement("soapenv:Envelope");
			envelope.setAttribute("xmlns:soapenv", "http://schemas.xmlsoap.org/soap/envelope/");
//			envelope.setAttribute("xmlns:epar", "http://SCLINSMSVM01T/wsdls/Surfline/EpartnerDataPurchase.wsdl");
//			envelope.setAttribute("xmlns:epar", applicationProperties.getXmlnsEpar());
			envelope.setAttribute("xmlns:epar", "http://SCLINSMSVM01T/wsdls/Surfline/EpartnerDataPurchase.wsdl");
			message.appendChild(envelope);
			
			Element header = message.createElement("soapenv:Header");
			envelope.appendChild(header);
			
			Element body = message.createElement("soapenv:Body");
			envelope.appendChild(body);
			
			Element ePartnerDataPurchaseRequest = message.createElement("epar:EpartnerDataPurchaseRequest");
			body.appendChild(ePartnerDataPurchaseRequest);
			
			Element ccCallingPartyId = message.createElement("CC_Calling_Party_Id");
			ccCallingPartyId.setTextContent(vCCCallingPartyId);
			ePartnerDataPurchaseRequest.appendChild(ccCallingPartyId);
			
			Element channel = message.createElement("CHANNEL");
			channel.setTextContent(vChannel);
			ePartnerDataPurchaseRequest.appendChild(channel);
			
			Element transactionId = message.createElement("TRANSACTION_ID");
			transactionId.setTextContent(vTransactionId);
			ePartnerDataPurchaseRequest.appendChild(transactionId);
			
			Element receipientNumber = message.createElement("Recipient_Number");
			receipientNumber.setTextContent(vReceipientNumber);
			ePartnerDataPurchaseRequest.appendChild(receipientNumber);
			
			Element receipientWalletType = message.createElement("RECEPIENT_WALLET_TYPE");
			receipientWalletType.setTextContent(vReceipientWalletType);
			ePartnerDataPurchaseRequest.appendChild(receipientWalletType);
			
			Element amount = message.createElement("AMOUNT");
			amount.setTextContent(vAmount);
			ePartnerDataPurchaseRequest.appendChild(amount);
			
			Element requestType = message.createElement("Request_type");
			requestType.setTextContent(vRequestType);
			ePartnerDataPurchaseRequest.appendChild(requestType);
			
			DOMSource domSource = new DOMSource(message);
			TransformerFactory tf = TransformerFactory.newInstance();
			
			Transformer transformer = tf.newTransformer();
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

			java.io.StringWriter sw = new java.io.StringWriter();
			StreamResult sr = new StreamResult(sw);
			transformer.transform(domSource, sr);
			responseXml = sw.toString();

			System.out.println(responseXml);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return responseXml;
	}

}
