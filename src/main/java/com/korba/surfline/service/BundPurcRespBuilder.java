package com.korba.surfline.service;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.korba.surfline.model.BundlePurchaseResponse;
import com.korba.surfline.properties.ApplicationProperties;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class BundPurcRespBuilder {
	@Autowired
	ApplicationProperties applicationProperties;

	public BundlePurchaseResponse produceResponse(String responseXml) {
		BundlePurchaseResponse bundlePurchaseResponse = new BundlePurchaseResponse();
		String serviceRequestId = "";
		String error="";

		try {
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
			Document document = documentBuilder.parse(new InputSource(new StringReader(responseXml)));
			document.getDocumentElement().normalize();

			NodeList errorList = document.getElementsByTagName("erorr");
			if(errorList.item(0) != null) {
				if (errorList.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element errorElement = (Element) errorList.item(0);
					error = errorElement.getTextContent();
					log.info(errorElement.getNodeName() + " : " + errorElement.getTextContent());
				}
			}
			
//			NodeList responses = document.getElementsByTagName("epar:EpartnerDataPurchaseResult");
//
//			for (int i = 0; i < responses.getLength(); i++) {
//				Node firstServiceNode = responses.item(i);
//
//				if (firstServiceNode.getNodeType() == Node.ELEMENT_NODE) {
//					Element element = (Element) firstServiceNode;
//
//					NodeList serviceRequestIdList = element.getElementsByTagName("ServiceRequestID");
//					Element serviceRequestIdElement = (Element) serviceRequestIdList.item(0);
//					serviceRequestId = serviceRequestIdElement.getTextContent();
//				}
//			}
			
			NodeList serviceRequestIdList = document.getElementsByTagName("ServiceRequestID");
			Element serviceRequestIdElement = (Element) serviceRequestIdList.item(0);
			serviceRequestId = serviceRequestIdElement.getTextContent();
			
			bundlePurchaseResponse.setSurflineTransId(serviceRequestId);
			bundlePurchaseResponse.setError(error);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return bundlePurchaseResponse;
	}

}
