package com.korba.surfline.service;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.korba.surfline.model.BalanceQueryResponse;
import com.korba.surfline.properties.ApplicationProperties;
import com.korba.surfline.utils.SOAPMessageReader;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class BalQueRespBuilder {
	@Autowired
	ApplicationProperties applicationProperties;
	@Autowired
	SOAPMessageReader reader;

	public BalanceQueryResponse produceBalQueryResponse(String responseXml) {
		BalanceQueryResponse response = new BalanceQueryResponse();
		String auth = "";
		String msisdn = "";
		String accountNumber = "";
		String balance = "";
		String error = "";
		String faultCode = "";
		String faultString = "";

		try {
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
			Document document = documentBuilder.parse(new InputSource(new StringReader(responseXml)));
			document.getDocumentElement().normalize();

			NodeList errorList = document.getElementsByTagName("erorr");
			if(errorList.item(0) != null) {
				if (errorList.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element errorElement = (Element) errorList.item(0);
					error = errorElement.getTextContent();
					log.info(errorElement.getNodeName() + " : " + errorElement.getTextContent());
				}
			}
			
			NodeList faultStringList = document.getElementsByTagName("faultstring");
			if(faultStringList.item(0) != null) {
				if (faultStringList.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element faultStringElement = (Element) faultStringList.item(0);
					faultString = faultStringElement.getTextContent();
					log.info(faultStringElement.getNodeName() + " : " + faultStringElement.getTextContent());
				}
			}
			NodeList faultCodeList = document.getElementsByTagName(applicationProperties.getPiPrefix()+"CODE");
			if(faultCodeList.item(0) != null) {
				if (faultCodeList.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element faultCodeElement = (Element) faultCodeList.item(0);
					faultCode = faultCodeElement.getTextContent();
					log.info(faultCodeElement.getNodeName() + " : " + faultCodeElement.getTextContent());
				}
			}
			
			NodeList authList = document.getElementsByTagName(applicationProperties.getPiPrefix()+"AUTH");
			Element authElement = (Element) authList.item(0);
			auth = authElement.getTextContent();
			
			NodeList msisdnList = document.getElementsByTagName(applicationProperties.getPiPrefix()+"MSISDN");
			Element msisdnElement = (Element) msisdnList.item(0);
			msisdn = msisdnElement.getTextContent();
			
			NodeList accountNumberList = document.getElementsByTagName(applicationProperties.getPiPrefix()+"ACCOUNT_NUMBER");
			Element accountNumberElement = (Element) accountNumberList.item(0);
			accountNumber = accountNumberElement.getTextContent();
			
			NodeList balanceList = document.getElementsByTagName(applicationProperties.getPiPrefix()+"BALANCE");
			Element balanceElement = (Element) balanceList.item(0);
			balance = balanceElement.getTextContent();
			
			response.setAuth(auth);
			response.setMsisdn(msisdn);
			response.setAccountNumber(accountNumber);
			response.setBalance(balance);
			response.setError(error);
			response.setFaultCode(faultCode);
			response.setFaultString(faultString);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return response;

	}
}
