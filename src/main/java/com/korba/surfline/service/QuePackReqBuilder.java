package com.korba.surfline.service;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.korba.surfline.properties.ApplicationProperties;

@Service
public class QuePackReqBuilder {
	@Autowired
	ApplicationProperties applicationProperties;
	
//	public static void main(String[] args) {
//		QuePackReqBuilder requestBuilder = new QuePackReqBuilder();
//		requestBuilder.getXmlRequest("Kel","Kel","Kel");
//	}

	public String getXmlRequest(String vUsername,String vPassword,String vMsisdn){
		String requestXml="";
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			DOMImplementation impl = builder.getDOMImplementation();

			Document message = impl.createDocument(null, null, null);
			
			Element envelope = message.createElement("soapenv:Envelope");
			envelope.setAttribute("xmlns:soapenv","http://schemas.xmlsoap.org/soap/envelope/");
//			envelope.setAttribute("xmlns:pac", "http://SCLINSMSVM01T/wsdls/Surfline/PackageQuery.wsdl");
			envelope.setAttribute("xmlns:pac", applicationProperties.getXmlnsPac());
			message.appendChild(envelope);
			
			Element header = message.createElement("soapenv:Header");
			envelope.appendChild(header);
			
			Element body = message.createElement("soapenv:Body");
			envelope.appendChild(body);
			
			Element packageQueryRequest = message.createElement("pac:PackageQueryRequest");
			body.appendChild(packageQueryRequest);
			
			Element username = message.createElement("pi:username");
			username.setTextContent(vUsername);
			packageQueryRequest.appendChild(username);
			
			Element password = message.createElement("pi:password");
			password.setTextContent(vPassword);
			packageQueryRequest.appendChild(password);
			
			Element msisdn = message.createElement("CC_Calling_Party_Id");
			msisdn.setTextContent(vMsisdn);
			packageQueryRequest.appendChild(msisdn);
			
			DOMSource domSource = new DOMSource(message);
			TransformerFactory tf = TransformerFactory.newInstance();
			
			Transformer transformer = tf.newTransformer();
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

			java.io.StringWriter sw = new java.io.StringWriter();
			StreamResult sr = new StreamResult(sw);
			transformer.transform(domSource, sr);
			requestXml = sw.toString();

			System.out.println(requestXml);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return requestXml;
	}
	

}
