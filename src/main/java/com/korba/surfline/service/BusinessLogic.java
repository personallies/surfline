package com.korba.surfline.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.korba.surfline.db.TransactionLogs;
import com.korba.surfline.model.BalanceQueryResponse;
import com.korba.surfline.model.BundlePurchaseRequest;
import com.korba.surfline.model.BundlePurchaseResponse;
import com.korba.surfline.model.QueryPackageRequest;
import com.korba.surfline.model.QueryPackageResponse;
import com.korba.surfline.model.QueryTransHistRequest;
import com.korba.surfline.model.ResponseCode;
import com.korba.surfline.model.ResponseMessage;
import com.korba.surfline.model.Status;
import com.korba.surfline.model.TransactionHistoryResponse;
import com.korba.surfline.properties.ApplicationProperties;
import com.korba.surfline.properties.MessageProperties;
import com.korba.surfline.utils.DBLogger;
import com.korba.surfline.utils.GeneralUtils;
import com.korba.surfline.utils.SOAPMessagePoster;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class BusinessLogic {
	@Autowired
	ApplicationProperties applicationProperties;
	@Autowired
	QuePackReqBuilder requestBuilder;
	@Autowired
	SOAPMessagePoster soapMessagePoster;
	@Autowired
	QuePackRespBuilder responseBuilder;
	@Autowired
	BundPurcReqBuilder bundPurcReqBuilder;
	@Autowired
	BundPurcRespBuilder bundPurcRespBuilder;
	@Autowired
	MessageProperties messageProperties;
	@Autowired
	GeneralUtils generalUtils;
	@Autowired
	DBLogger dbLogger;
	@Autowired
	BalQueReqBuilder balQueReqBuilder;
	@Autowired
	BalQueRespBuilder balQueRespBuilder;
	@Autowired
	TransHistReqBuilder transHistReqBuilder;
	@Autowired
	TransHistRespBuilder transHistRespBuilder;

	public QueryPackageResponse queryPackage(QueryPackageRequest request) {
		QueryPackageResponse response = new QueryPackageResponse();
		// String vUsername = applicationProperties.getUsername();
		String vUsername = applicationProperties.getSurflineUsername();
		String vPassword = applicationProperties.getSurflinePassword();
		String vMsisdn = request.getMsisdn();
		String endpointUrl = applicationProperties.getPackageQueryUrl();

		log.info("the username " + vUsername);

		String requestXml = requestBuilder.getXmlRequest(vUsername, vPassword, vMsisdn);
		log.info("Going to " + applicationProperties.getPackageQueryUrl() + " for a response");
		String responseXml = soapMessagePoster.sendMessage(endpointUrl, requestXml);

		System.out.println(responseXml);
		response = responseBuilder.producePacQueryResponse(responseXml, applicationProperties.getNamespace());

		if (Status.SUCCESS.equalsIgnoreCase(response.getResult())) {
			response.setResponseCode(ResponseCode.SUCCESS);
			response.setResponseMessage(ResponseMessage.QUERY_SUCCESS);
		} else {
			response.setResponseCode(ResponseCode.FAIL);
			response.setResponseMessage(ResponseMessage.QUERY_FAIL);
		}
		return response;
	}

	public BundlePurchaseResponse purchaseBundle(BundlePurchaseRequest bundlePurchaseRequest) throws Exception {
		BundlePurchaseResponse bundlePurchaseResponse = new BundlePurchaseResponse();
		TransactionLogs savedTransaction = null;
		String responseXml = "";
		String vCCCallingPartyId = applicationProperties.getCcCallingPartyId();
		String vChannel = applicationProperties.getChannel();
		// String vTransactionId = TransactionIdGenerator.nextId().toString();
		String vTransactionId = bundlePurchaseRequest.getRefNo();
		String vReceipientNumber = bundlePurchaseRequest.getMsisdn();
		String vReceipientWalletType = applicationProperties.getRecipientWalletType();
		String vAmount = bundlePurchaseRequest.getAmount();
		String vRequestType = applicationProperties.getRequestType();
		// String korbaTransId = TransactionIdGenerator.nextId().toString();
		String korbaTransId = bundlePurchaseRequest.getRefNo();

		// log the transaction
		savedTransaction = dbLogger.logTransaction(korbaTransId, bundlePurchaseRequest);

		String requestXml = bundPurcReqBuilder.getXmlRequest(vCCCallingPartyId, vChannel, vTransactionId,
				vReceipientNumber, vReceipientWalletType, vAmount, vRequestType);
		log.info("Going to " + applicationProperties.getBundlePurchaseUrl() + " for a response");

		responseXml = soapMessagePoster.postSoapMessageWithBasicAuth(applicationProperties.getBundlePurchaseUrl(),
				requestXml, applicationProperties.getSurflineUsername(), applicationProperties.getSurflinePassword());

		System.out.println(responseXml);
		bundlePurchaseResponse = bundPurcRespBuilder.produceResponse(responseXml);

		log.info("The Json response : " + bundlePurchaseResponse);
		bundlePurchaseResponse.setKorbaTransId(korbaTransId);
		if (!bundlePurchaseResponse.getSurflineTransId().isEmpty()) {
			bundlePurchaseResponse.setResponseCode(ResponseCode.SUCCESS);
			bundlePurchaseResponse.setResponseMessage(ResponseMessage.PURCHASE_SUCCESS);
		} else {
			bundlePurchaseResponse.setResponseCode(ResponseCode.FAIL);
			bundlePurchaseResponse.setResponseMessage(ResponseMessage.PURCHASE_FAIL);
		}

		// update the transaction with the response
		dbLogger.updateTransaction(savedTransaction, bundlePurchaseResponse);
		return bundlePurchaseResponse;
	}

	public BalanceQueryResponse queryBalance() {
		BalanceQueryResponse balanceQueryResponse = new BalanceQueryResponse();
		String vUsername = applicationProperties.getSurflineUsername();
		String vPassword = applicationProperties.getSurflinePassword();
		String vMsisdn = applicationProperties.getMsisdn();
		String vListType = applicationProperties.getListType();
		String vWalletType = applicationProperties.getWalletType();
		String vBalanceType = applicationProperties.getBalanceType();

		String requestXml = balQueReqBuilder.getXmlRequest(vUsername, vPassword, vMsisdn, vListType, vWalletType,
				vBalanceType);
		String url = applicationProperties.getBalanceQueryUrl();
		log.info("Going to " + url + " for a response");

		String responseXml = soapMessagePoster.sendMessage(url, requestXml);
		System.out.println(responseXml);

		balanceQueryResponse = balQueRespBuilder.produceBalQueryResponse(responseXml);

		log.info("The Json response : " + balanceQueryResponse);
		if (!balanceQueryResponse.getBalance().isEmpty()) {
			balanceQueryResponse.setResponseCode(ResponseCode.SUCCESS);
			balanceQueryResponse.setResponseMessage(ResponseMessage.BAL_QUERY_SUCCESS);
		} else {
			balanceQueryResponse.setResponseCode(ResponseCode.FAIL);
			balanceQueryResponse.setResponseMessage(ResponseMessage.BAL_QUERY_FAIL);
		}

		return balanceQueryResponse;
	}

	public TransactionHistoryResponse queryTransactionHistory(QueryTransHistRequest request) {
		TransactionHistoryResponse transactionHistoryResponse = new TransactionHistoryResponse();
		DateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd");
		String vUsername = applicationProperties.getSurflineUsername();
		String vPassword = applicationProperties.getSurflinePassword();
		String vMsisdn = applicationProperties.getMsisdn();
		String vWalletType = applicationProperties.getWalletType();
		String vEdrType = applicationProperties.getEdrType();
		String vMaxRecords = applicationProperties.getMaxRecords();
		String vStartDate;
		String vEndDate;

		if (request.getStartDate() != null) {
			vStartDate = request.getStartDate();
		}
		else {
			vStartDate = applicationProperties.getStartDate();
		}

		if (request.getEndDate() != null) {
			vEndDate = request.getEndDate();
		}
		else {
			Date now = new Date();
			vEndDate = dateFormat.format(now);
		}

		if (vStartDate.contains("-")) {
			vStartDate = vStartDate.replace("-", "");
			vStartDate = vStartDate.concat("000000");
		} else if (!vStartDate.endsWith("000000")) {
			vStartDate = vStartDate.concat("000000");
		}

		if (vEndDate.contains("-")) {
			vEndDate = vEndDate.replace("-", "");
			vEndDate = vEndDate.concat("000000");
		} else if (!vEndDate.endsWith("000000")) {
			vEndDate = vEndDate.concat("000000");
		}

		String requestXml = transHistReqBuilder.getXmlRequest(vUsername, vPassword, vMsisdn, vWalletType, vEdrType,
				vMaxRecords, vStartDate, vEndDate);
		String url = applicationProperties.getTransactionHistoryUrl();
		log.info("Going to " + url + " for a response");

		String responseXml = soapMessagePoster.sendMessage(url, requestXml);
		System.out.println(responseXml);

		transactionHistoryResponse = transHistRespBuilder.produceTransHistResponse(responseXml);

		log.info("The Json response : " + transactionHistoryResponse);
		if (!transactionHistoryResponse.getAuth().isEmpty()) {
			transactionHistoryResponse.setResponseCode(ResponseCode.SUCCESS);
			transactionHistoryResponse.setResponseMessage(ResponseMessage.TRANS_HIST_QUERY_SUCCESS);
		} else {
			transactionHistoryResponse.setResponseCode(ResponseCode.FAIL);
			transactionHistoryResponse.setResponseMessage(ResponseMessage.TRANS_HIST_QUERY_FAIL);
		}

		return transactionHistoryResponse;
	}
}
