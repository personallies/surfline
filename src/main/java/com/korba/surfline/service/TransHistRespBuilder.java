package com.korba.surfline.service;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.korba.surfline.model.EDR;
import com.korba.surfline.model.TransactionHistoryResponse;
import com.korba.surfline.properties.ApplicationProperties;
import com.korba.surfline.utils.SOAPMessageReader;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class TransHistRespBuilder {
	@Autowired
	ApplicationProperties applicationProperties;
	@Autowired
	SOAPMessageReader reader;

	public TransactionHistoryResponse produceTransHistResponse(String responseXml) {
		TransactionHistoryResponse response = new TransactionHistoryResponse();
		List<EDR> tmpEdrList = new ArrayList<>();
		EDR edrItem = new EDR();
		String auth = "";
		String msisdn = "";
		String error = "";
		String faultString  = "";
		String faultCode = "";

		try {
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
			Document document = documentBuilder.parse(new InputSource(new StringReader(responseXml)));
			document.getDocumentElement().normalize();

			NodeList errorList = document.getElementsByTagName("erorr");
			if(errorList.item(0) != null) {
				if (errorList.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element errorElement = (Element) errorList.item(0);
					error = errorElement.getTextContent();
					log.info(errorElement.getNodeName() + " : " + errorElement.getTextContent());
				}
			}
			
			NodeList faultStringList = document.getElementsByTagName("faultstring");
			if(faultStringList.item(0) != null) {
				if (faultStringList.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element faultStringElement = (Element) faultStringList.item(0);
					faultString = faultStringElement.getTextContent();
					log.info(faultStringElement.getNodeName() + " : " + faultStringElement.getTextContent());
				}
			}
			
			NodeList faultCodeList = document.getElementsByTagName(applicationProperties.getPiPrefix()+"CODE");
			if(faultCodeList.item(0) != null) {
				if (faultCodeList.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element faultCodeElement = (Element) faultCodeList.item(0);
					faultCode = faultCodeElement.getTextContent();
					log.info(faultCodeElement.getNodeName() + " : " + faultCodeElement.getTextContent());
				}
			}
			
			NodeList authList = document.getElementsByTagName(applicationProperties.getPiPrefix()+"AUTH");
			if(authList.item(0) != null) {
				if (authList.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element authElement = (Element) authList.item(0);
					auth = authElement.getTextContent();
					log.info(authElement.getNodeName() + " : " + authElement.getTextContent());
				}
			}
			
			NodeList msisdnList = document.getElementsByTagName(applicationProperties.getPiPrefix()+"MSISDN");
			if(msisdnList.item(0) != null) {
				if (msisdnList.item(0).getNodeType() == Node.ELEMENT_NODE) {
					Element msisdnElement = (Element) authList.item(0);
					msisdn = msisdnElement.getTextContent();
					log.info(msisdnElement.getNodeName() + " : " + msisdnElement.getTextContent());
				}
			}
			
			NodeList edrItemList = document.getElementsByTagName(applicationProperties.getPiPrefix()+"EDR_ITEM");
			if(edrItemList != null) {
				
				for(int i=0;i<edrItemList.getLength();i++) {
					Node nNode = edrItemList.item(i);
					
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {
						Element eElement = (Element) nNode;
						
						edrItem.setRecordDate(eElement.getElementsByTagName(applicationProperties.getPiPrefix()+"RECORD_DATE").item(0).getTextContent());
						edrItem.setEdrType(eElement.getElementsByTagName(applicationProperties.getPiPrefix()+"EDR_TYPE").item(0).getTextContent());
						edrItem.setWalletType(eElement.getElementsByTagName(applicationProperties.getPiPrefix()+"WALLET_TYPE").item(0).getTextContent());
						edrItem.setChargingDomainId(eElement.getElementsByTagName(applicationProperties.getPiPrefix()+"CHARGING_DOMAIN_ID").item(0).getTextContent());
						edrItem.setCallId(eElement.getElementsByTagName(applicationProperties.getPiPrefix()+"CALL_ID").item(0).getTextContent());
						edrItem.setScpId(eElement.getElementsByTagName(applicationProperties.getPiPrefix()+"SCP_ID").item(0).getTextContent());
						edrItem.setSequenceNumber(eElement.getElementsByTagName(applicationProperties.getPiPrefix()+"SEQUENCE_NUMBER").item(0).getTextContent());
						edrItem.setExtraInformation(eElement.getElementsByTagName(applicationProperties.getPiPrefix()+"EXTRA_INFORMATION").item(0).getTextContent());
						tmpEdrList.add(edrItem);
					}
				}
			}

			response.setAuth(auth);
			response.setMsisdn(msisdn);
			response.setEdrList(tmpEdrList);
			response.setError(error);
			response.setFaultCode(faultCode);
			response.setFaultString(faultString);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return response;

	}
}
